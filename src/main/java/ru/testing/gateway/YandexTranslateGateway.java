package ru.testing.gateway;

import com.google.gson.Gson;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONArray;
import ru.testing.entities.Translate;

@Slf4j
public class YandexTranslateGateway {
    private static final String URL = "https://translate.api.cloud.yandex.net/translate/v2/translate";
    private static final String TOKEN = "t1.9euelZqejY_KmceJmM7Hjp2MlonNku3rnpWakZrInZ6XyouVmI2KjsecmZrl8_d6VQ12-e9dCT8A_t3z9zoEC3b5710JPwD-.1D9mYwdi55CteFrjiyqMsaaG723u1yNKzVzSJt40Gyiv7ho-ahAD8Ny7CtjxZp_Ox5vDsmsS3Q7bg-tiFFe0BA";

    @SneakyThrows
    public Translate getTranslate(String text) {
        String [] myArray = text.split(" ");
        String folderId = "b1gdh4rp2s0tckc40bk9";
        JSONArray jsArray = new JSONArray();
        for (int i = 0; i < myArray.length; i++) {
            jsArray.put(myArray[i]);
        }

        Gson gson = new Gson();

        HttpResponse<JsonNode> response = Unirest.post(URL)
                .header("Accept", "*/*")
                .header("Authorization", "Bearer "+TOKEN)
                .body("{\"folderId\": \""+ folderId+ "\",\"texts\":" +jsArray +",\"targetLanguageCode\": \"ru\"\n}")
                .asJson();
        JsonNode strResponse = response.getBody();
        log.info("response: "+strResponse);
        log.info(text.split(" ")[0]);
        return gson.fromJson(String.valueOf(strResponse), Translate.class);
    }
}