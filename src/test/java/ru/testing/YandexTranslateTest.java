package ru.testing;

import org.junit.jupiter.api.DisplayName;
import ru.testing.entities.Translate;
import ru.testing.gateway.YandexTranslateGateway;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class YandexTranslateTest {
    private static final String HELLO_WORLD = "������ ���";

    @Test
    @DisplayName("������� Hello World")
    public void getTranslate(){
        YandexTranslateGateway yandexTranslateGateway = new YandexTranslateGateway();
        Translate translate = yandexTranslateGateway.getTranslate("Hello World");
        String [] result = { translate.getTranslations()[0].getText(), translate.getTranslations()[1].getText()};
        Assertions.assertEquals (String.join(" ",result),HELLO_WORLD);
    }
}